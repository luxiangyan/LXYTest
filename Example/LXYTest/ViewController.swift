//
//  ViewController.swift
//  LXYTest
//
//  Created by LXY on 11/30/2018.
//  Copyright (c) 2018 LXY. All rights reserved.
//

import UIKit
import LXYTest

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let vc = LXYViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

